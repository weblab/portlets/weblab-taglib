# WebLab Taglib

This project is a Maven based Java project of a Taglib.
This utility taglib can be used inside WebLab portlet for some commodity methods and in order to display properly values of RDF properties inside WebLab Resources. 

# Build status
## Master
[![build status](https://gitlab.ow2.org/weblab/portlets/weblab-taglib/badges/master/build.svg)](https://gitlab.ow2.org/weblab/portlets/weblab-taglib/commits/master)
## Develop
[![build status](https://gitlab.ow2.org/weblab/portlets/weblab-taglib/badges/develop/build.svg)](https://gitlab.ow2.org/weblab/portlets/weblab-taglib/commits/develop)

# How to build it

In order to be build it, you need to have access to the Maven dependencies it is using. Most of the dependencies are in the central repository and thus does not implies specific configuration.
However, the WebLab Core dependencies are not yet included in the Maven central repository but in a dedicated one that we manage ourselves.
Thus you may have to add the repositories that are listed in the settings.xml. 
