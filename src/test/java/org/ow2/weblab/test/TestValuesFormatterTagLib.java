/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.test;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.ow2.weblab.components.taglib.ValuesFormatterTagLib;
import org.springframework.mock.web.MockBodyContent;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockPageContext;
import org.springframework.mock.web.MockServletContext;


/**
 * Class to test the formatter taglib
 *
 * @author emilienbondu
 */
public class TestValuesFormatterTagLib {


	private MockServletContext mockServletContext;


	private MockPageContext mockPageContext;


	private MockBodyContent mockBodyContent;


	private static ResourceBundle resourceBundle = ResourceBundle.getBundle("weblab_taglib", Locale.ENGLISH);


	@Before
	public void setup() {

		this.mockServletContext = new MockServletContext();
		this.mockPageContext = new MockPageContext(this.mockServletContext);
	}


	@Test
	public void testFormatIconSimpleValue() throws JspException, UnsupportedEncodingException {

		final ValuesFormatterTagLib lib = new ValuesFormatterTagLib();

		lib.setValue("application/xml");
		lib.setStyle("icon");
		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		Assert.assertEquals("application/xml", lib.getValue());
		lib.doEndTag();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		Assert.assertEquals("<img src='" + TestValuesFormatterTagLib.resourceBundle.getString("taglib.icon.xml") + "' alt='" + TestValuesFormatterTagLib.resourceBundle.getString("taglib.text.xml")
				+ "' title='" + TestValuesFormatterTagLib.resourceBundle.getString("taglib.text.xml") + "'/>", output);
	}


	@Test
	public void testFormatIconListValue() throws JspException, UnsupportedEncodingException {

		final ValuesFormatterTagLib lib = new ValuesFormatterTagLib();


		lib.setValue(Arrays.asList("application/xml", "application/not-defined-to-test-octet-stream"));
		lib.setStyle("icon");
		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doEndTag();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		Assert.assertEquals("<img src='" + TestValuesFormatterTagLib.resourceBundle.getString("taglib.icon.xml") + "' alt='" + TestValuesFormatterTagLib.resourceBundle.getString("taglib.text.xml")
				+ "' title='" + TestValuesFormatterTagLib.resourceBundle.getString("taglib.text.xml") + "'/> <img src='" + TestValuesFormatterTagLib.resourceBundle.getString("taglib.icon.unknown")
				+ "' alt='application/not-defined-to-test-octet-stream' title='application/not-defined-to-test-octet-stream'/> ", output);

	}


	@Test
	public void testFormatIconSimpleValueFromBody() throws UnsupportedEncodingException {

		final ValuesFormatterTagLib lib = new ValuesFormatterTagLib();

		this.mockBodyContent = new MockBodyContent("application/xml", (HttpServletResponse) this.mockPageContext.getResponse());
		lib.setBodyContent(this.mockBodyContent);
		lib.setStyle("icon");
		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doAfterBody();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		Assert.assertEquals("<img src='" + TestValuesFormatterTagLib.resourceBundle.getString("taglib.icon.xml") + "' alt='" + TestValuesFormatterTagLib.resourceBundle.getString("taglib.text.xml")
				+ "' title='" + TestValuesFormatterTagLib.resourceBundle.getString("taglib.text.xml") + "'/>", output);
	}


	@Test
	public void testFormatIconPlusTextValueFromBody() throws UnsupportedEncodingException {

		final ValuesFormatterTagLib lib = new ValuesFormatterTagLib();

		this.mockBodyContent = new MockBodyContent("application/msword", (HttpServletResponse) this.mockPageContext.getResponse());
		lib.setBodyContent(this.mockBodyContent);
		lib.setStyle("icon+text");
		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doAfterBody();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		Assert.assertEquals("<img src='" + TestValuesFormatterTagLib.resourceBundle.getString("taglib.icon.msword") + "' alt='"
				+ TestValuesFormatterTagLib.resourceBundle.getString("taglib.text.msword") + "' title='" + TestValuesFormatterTagLib.resourceBundle.getString("taglib.text.msword") + "'/><span>"
				+ TestValuesFormatterTagLib.resourceBundle.getString("taglib.text.msword") + "</span>", output);
	}


	@Test
	public void testFormatIconSimpleValueFromBody2() throws UnsupportedEncodingException {

		final ValuesFormatterTagLib lib = new ValuesFormatterTagLib();

		this.mockBodyContent = new MockBodyContent("application/x-twitter", (HttpServletResponse) this.mockPageContext.getResponse());
		lib.setBodyContent(this.mockBodyContent);
		lib.setStyle("icon");
		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doAfterBody();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		Assert.assertEquals("<img src='" + TestValuesFormatterTagLib.resourceBundle.getString("taglib.icon.twitter") + "' alt='"
				+ TestValuesFormatterTagLib.resourceBundle.getString("taglib.text.twitter") + "' title='" + TestValuesFormatterTagLib.resourceBundle.getString("taglib.text.twitter") + "'/>", output);
	}


	@Test
	public void testFormatTextSimpleValueFromBody() throws UnsupportedEncodingException {

		final ValuesFormatterTagLib lib = new ValuesFormatterTagLib();

		this.mockBodyContent = new MockBodyContent("application/xml", (HttpServletResponse) this.mockPageContext.getResponse());
		lib.setBodyContent(this.mockBodyContent);
		lib.setStyle("text");
		lib.setCssClass("cssClass");
		lib.setCssId("cssId");
		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();

		Assert.assertEquals("text", lib.getStyle());
		Assert.assertEquals("cssClass", lib.getCssClass());
		Assert.assertEquals("cssId", lib.getCssId());

		lib.doAfterBody();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		Assert.assertEquals("<span class='cssClass' id='cssId'>" + TestValuesFormatterTagLib.resourceBundle.getString("taglib.text.xml") + "</span>", output);
	}


	@Test
	public void testFormat1023FileSizeValueFromBody() throws UnsupportedEncodingException {

		final ValuesFormatterTagLib lib = new ValuesFormatterTagLib();

		this.mockBodyContent = new MockBodyContent("1023", (HttpServletResponse) this.mockPageContext.getResponse());
		lib.setBodyContent(this.mockBodyContent);
		lib.setStyle("fileSize");
		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doAfterBody();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		Assert.assertEquals("<span>1023 bytes</span>", output);
	}


	@Test
	public void testFormat2000FileSizeValueFromBody() throws UnsupportedEncodingException {

		final ValuesFormatterTagLib lib = new ValuesFormatterTagLib();

		this.mockBodyContent = new MockBodyContent("2000", (HttpServletResponse) this.mockPageContext.getResponse());
		lib.setBodyContent(this.mockBodyContent);
		lib.setStyle("fileSize");
		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doAfterBody();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		Assert.assertTrue(output.startsWith("<span>1") && output.endsWith("95 KB</span>"));
	}


	@Test
	public void testFormat20000000FileSizeValueFromBody() throws UnsupportedEncodingException {

		final ValuesFormatterTagLib lib = new ValuesFormatterTagLib();

		this.mockBodyContent = new MockBodyContent("20000000", (HttpServletResponse) this.mockPageContext.getResponse());
		lib.setBodyContent(this.mockBodyContent);
		lib.setStyle("fileSize");
		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doAfterBody();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		Assert.assertTrue(output.startsWith("<span>19") && output.endsWith("07 MB</span>"));
	}


	@Test
	public void testFormatMaxLongFileSizeValueFromBody() throws UnsupportedEncodingException {

		final ValuesFormatterTagLib lib = new ValuesFormatterTagLib();

		this.mockBodyContent = new MockBodyContent(Long.toString(Long.MAX_VALUE), (HttpServletResponse) this.mockPageContext.getResponse());
		lib.setBodyContent(this.mockBodyContent);
		lib.setStyle("fileSize");
		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doAfterBody();

		((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
	}


	@Test
	public void testFormatLongFileSizeValue() throws JspException, UnsupportedEncodingException {

		final ValuesFormatterTagLib lib = new ValuesFormatterTagLib();

		lib.setValue(Long.valueOf(2048));
		lib.setStyle("fileSize");
		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doEndTag();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		Assert.assertEquals("<span>2 KB</span>", output);
	}


	@Test
	public void testFormatLongFileSizeListValue() throws JspException, UnsupportedEncodingException {

		final ValuesFormatterTagLib lib = new ValuesFormatterTagLib();

		lib.setValue(Arrays.asList(Long.valueOf(2048), Long.valueOf(4096)));
		lib.setStyle("fileSize");
		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doEndTag();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		Assert.assertEquals("<span>2 KB</span> <span>4 KB</span> ", output);
	}


	@Test
	public void testFormatLongNotFileSizeValue() throws JspException, UnsupportedEncodingException {

		final ValuesFormatterTagLib lib = new ValuesFormatterTagLib();

		lib.setValue(Long.valueOf(2048));
		lib.setStyle("text");
		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doEndTag();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		Assert.assertEquals("<span>2048</span>", output);
	}


	@Test
	public void testFormatLongNotFileSizeListValue() throws JspException, UnsupportedEncodingException {

		final ValuesFormatterTagLib lib = new ValuesFormatterTagLib();

		lib.setValue(Arrays.asList(Long.valueOf(2048), Long.valueOf(4096)));
		lib.setStyle("text");
		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doEndTag();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		Assert.assertEquals("<span>2048</span> <span>4096</span> ", output);
	}


	@Test
	public void testFormatNotLongNotFileSizeListValue() throws JspException, UnsupportedEncodingException {

		final ValuesFormatterTagLib lib = new ValuesFormatterTagLib();

		lib.setValue(Arrays.asList(Double.valueOf(2048), Double.valueOf(4096)));
		lib.setStyle("text");
		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doEndTag();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		Assert.assertEquals("<span>2048.0</span> <span>4096.0</span> ", output);
	}


	@Test
	public void testFormatNotLongFileSizeListValue() throws JspException, UnsupportedEncodingException {

		final ValuesFormatterTagLib lib = new ValuesFormatterTagLib();

		lib.setValue(Arrays.asList(Double.valueOf(2048), Double.valueOf(4096)));
		lib.setStyle("fileSize");
		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doEndTag();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		// This fails!
		Assert.assertEquals("  ", output);
	}


	@Test
	public void testFormatNullValue() throws JspException, UnsupportedEncodingException {

		final ValuesFormatterTagLib lib = new ValuesFormatterTagLib();

		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doEndTag();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		Assert.assertEquals("", output);
	}


	@Test
	public void testAnchorFromEmptyBody() throws UnsupportedEncodingException {

		final ValuesFormatterTagLib lib = new ValuesFormatterTagLib();

		this.mockBodyContent = new MockBodyContent(" ", (HttpServletResponse) this.mockPageContext.getResponse());
		lib.setBodyContent(this.mockBodyContent);
		lib.setStyle("anchor");
		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doAfterBody();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		Assert.assertEquals("", output);
	}


	@Test
	public void testNotAnchorFromBody() throws UnsupportedEncodingException {

		final ValuesFormatterTagLib lib = new ValuesFormatterTagLib();

		this.mockBodyContent = new MockBodyContent("not an anchor", (HttpServletResponse) this.mockPageContext.getResponse());
		lib.setBodyContent(this.mockBodyContent);
		lib.setStyle("anchor");
		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doAfterBody();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		Assert.assertEquals("not an anchor", output);
	}


	@Test
	public void testAnchorsFromBody() throws UnsupportedEncodingException {

		for (final String validLink : Arrays.asList("http://perdu.com", "https://perdu.com", "ftp://perdu.com", "ftps://perdu.com")) {

			final ValuesFormatterTagLib lib = new ValuesFormatterTagLib();

			this.mockServletContext = new MockServletContext();
			this.mockPageContext = new MockPageContext(this.mockServletContext);

			this.mockBodyContent = new MockBodyContent(validLink, (HttpServletResponse) this.mockPageContext.getResponse());
			lib.setBodyContent(this.mockBodyContent);
			lib.setStyle("anchor");
			lib.setPageContext(this.mockPageContext);

			lib.doStartTag();
			lib.doAfterBody();

			final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
			Assert.assertEquals("<a href='" + validLink + "'  target='_blank' >" + validLink + "</a>", output);
		}
	}


	@Test
	public void testNotAnchorButForcedFromBody() throws UnsupportedEncodingException {

		final ValuesFormatterTagLib lib = new ValuesFormatterTagLib();

		this.mockBodyContent = new MockBodyContent("not an anchor but forced", (HttpServletResponse) this.mockPageContext.getResponse());
		lib.setBodyContent(this.mockBodyContent);
		lib.setStyle("anchor+forced");
		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doAfterBody();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		Assert.assertEquals("<a href='not an anchor but forced'  target='_blank' >not an anchor but forced</a>", output);
	}


}
