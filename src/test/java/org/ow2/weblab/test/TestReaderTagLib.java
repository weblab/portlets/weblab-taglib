/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.test;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import javax.servlet.jsp.JspException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.ow2.weblab.components.taglib.PropertyReaderTagLib;
import org.ow2.weblab.core.extended.ontologies.DublinCore;
import org.ow2.weblab.core.extended.ontologies.WebLabProcessing;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.purl.dc.elements.DublinCoreAnnotator;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockPageContext;
import org.springframework.mock.web.MockServletContext;


/**
 * Class to test the reader taglib
 *
 * @author emilienbondu
 *
 */
public class TestReaderTagLib {


	private MockServletContext mockServletContext;


	private MockPageContext mockPageContext;


	@Before
	public void setup() {

		this.mockServletContext = new MockServletContext();
		this.mockPageContext = new MockPageContext(this.mockServletContext);
	}


	@Test
	public void testPrintSimpleValue() throws JspException, UnsupportedEncodingException {

		final PropertyReaderTagLib lib = new PropertyReaderTagLib();
		final Document doc = new Document();
		doc.setUri("http://" + UUID.randomUUID().toString());

		final DublinCoreAnnotator dca = new DublinCoreAnnotator(doc);
		dca.writeTitle("mon titre");

		lib.setResource(doc);
		lib.setUri(DublinCore.TITLE_PROPERTY_NAME);

		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();

		Assert.assertSame(doc, lib.getResource());
		Assert.assertSame(DublinCore.TITLE_PROPERTY_NAME, lib.getUri());
		lib.doEndTag();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		Assert.assertEquals("mon titre", output);
	}


	@Test
	public void testPrintSimpleValueNotExisst() throws JspException, UnsupportedEncodingException {

		final PropertyReaderTagLib lib = new PropertyReaderTagLib();
		final Document doc = new Document();
		doc.setUri("http://" + UUID.randomUUID().toString());

		final DublinCoreAnnotator dca = new DublinCoreAnnotator(doc);
		dca.writeTitle("mon titre");

		lib.setResource(doc);
		lib.setUri(DublinCore.CONTRIBUTOR_PROPERTY_NAME);

		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doEndTag();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		Assert.assertTrue(output.isEmpty());
	}


	@Test
	public void testPrintSimplePageContext() throws JspException, UnsupportedEncodingException {

		final PropertyReaderTagLib lib = new PropertyReaderTagLib();
		final Document doc = new Document();
		doc.setUri("http://" + UUID.randomUUID().toString());

		final DublinCoreAnnotator dca = new DublinCoreAnnotator(doc);
		dca.writeTitle("mon titre");

		this.mockPageContext.getRequest().setAttribute("resource", doc);

		lib.setUri(DublinCore.TITLE_PROPERTY_NAME);

		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doEndTag();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		Assert.assertTrue(output.contains("mon titre"));
	}


	@Test
	public void testPrintTypedSimpleValue() throws JspException, UnsupportedEncodingException {

		final PropertyReaderTagLib lib = new PropertyReaderTagLib();
		final Document doc = new Document();
		doc.setUri("http://" + UUID.randomUUID().toString());

		final Date now = new Date();
		final WProcessingAnnotator wpa = new WProcessingAnnotator(doc);
		wpa.writeGatheringDate(now);

		lib.setResource(doc);
		lib.setUri(WebLabProcessing.HAS_GATHERING_DATE);

		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doEndTag();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		Assert.assertTrue(output.contains(now.toString()));
	}


	@Test
	public void testPrintLanguageDependentValue() throws JspException, UnsupportedEncodingException {

		final PropertyReaderTagLib lib = new PropertyReaderTagLib();
		final Document doc = new Document();
		doc.setUri("http://" + UUID.randomUUID().toString());

		final DublinCoreAnnotator dca = new DublinCoreAnnotator(doc);
		dca.writeTitle("mon titre");
		dca.startSpecifyLanguage("en");
		dca.writeTitle("my title");

		final WProcessingAnnotator wpa = new WProcessingAnnotator(doc);
		wpa.writeGatheringDate(new Date());

		this.mockPageContext.getResponse().setLocale(Locale.ENGLISH);
		lib.setResource(doc);
		lib.setUri(DublinCore.TITLE_PROPERTY_NAME);

		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doEndTag();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		Assert.assertTrue(output.contains("my title"));
		Assert.assertFalse(output.contains("mon titre"));
	}


	@Test
	public void testPrintLanguageDependentValues() throws JspException, UnsupportedEncodingException {

		final PropertyReaderTagLib lib = new PropertyReaderTagLib();
		final Document doc = new Document();
		doc.setUri("http://" + UUID.randomUUID().toString());

		final DublinCoreAnnotator dca = new DublinCoreAnnotator(doc);
		dca.writeTitle("mon titre");
		dca.startSpecifyLanguage("en");
		dca.writeTitle("my title");
		dca.writeTitle("my title2");

		final WProcessingAnnotator wpa = new WProcessingAnnotator(doc);
		wpa.writeGatheringDate(new Date());

		this.mockPageContext.getResponse().setLocale(Locale.ENGLISH);
		lib.setResource(doc);
		lib.setUri(DublinCore.TITLE_PROPERTY_NAME);

		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doEndTag();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		Assert.assertTrue(output.contains("my title"));
		Assert.assertTrue(output.contains("my title2"));
		Assert.assertFalse(output.contains("mon titre"));
	}


	@Test
	public void testPrintNoLanguageDependentValues() throws JspException, UnsupportedEncodingException {

		final PropertyReaderTagLib lib = new PropertyReaderTagLib();
		final Document doc = new Document();
		doc.setUri("http://" + UUID.randomUUID().toString());

		final DublinCoreAnnotator dca = new DublinCoreAnnotator(doc);
		dca.writeTitle("mon titre");
		dca.startSpecifyLanguage("en");
		dca.writeTitle("my title");
		dca.writeTitle("my title2");

		final WProcessingAnnotator wpa = new WProcessingAnnotator(doc);
		wpa.writeGatheringDate(new Date());

		this.mockPageContext.getResponse().setLocale(Locale.FRENCH);
		lib.setResource(doc);
		lib.setUri(DublinCore.TITLE_PROPERTY_NAME);

		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doEndTag();

		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		Assert.assertTrue(output.contains("mon titre"));
		Assert.assertTrue(output.contains("my title"));
		Assert.assertTrue(output.contains("my title2"));
	}


	// / writing in page context
	@Test
	public void testAddAttributeSimpleValue() throws JspException {

		final PropertyReaderTagLib lib = new PropertyReaderTagLib();
		final Document doc = new Document();
		final Text mu = new Text();
		mu.setUri("http://_" + UUID.randomUUID().toString());
		doc.getMediaUnit().add(mu);
		doc.setUri("http://" + UUID.randomUUID().toString());

		final DublinCoreAnnotator dca = new DublinCoreAnnotator(doc);
		dca.writeTitle("mon titre");

		final DublinCoreAnnotator dca2 = new DublinCoreAnnotator(mu);
		dca2.startInnerAnnotatorOn(URI.create(doc.getUri()));


		dca2.writeTitle("mon titre2");

		lib.setResource(doc);

		lib.setUri(DublinCore.TITLE_PROPERTY_NAME);
		lib.setVar("title");
		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		Assert.assertEquals("title", lib.getVar());
		lib.doEndTag();


		final List<?> titles = (List<?>) this.mockPageContext.getAttribute("title");
		Assert.assertEquals(2, titles.size());
		Assert.assertTrue(titles.get(0) instanceof String);
		Assert.assertTrue((((String) titles.get(0)).contains("mon titre") && ((String) titles.get(1)).contains("mon titre2"))
				|| (((String) titles.get(0)).contains("mon titre2") && ((String) titles.get(1)).contains("mon titre")));
	}


	@Test
	public void testAddAttributeTypedSimpleValue() throws JspException {

		final PropertyReaderTagLib lib = new PropertyReaderTagLib();
		final Document doc = new Document();
		doc.setUri("http://" + UUID.randomUUID().toString());

		final Date now = new Date();
		final WProcessingAnnotator wpa = new WProcessingAnnotator(doc);
		wpa.writeGatheringDate(now);

		lib.setResource(doc);
		lib.setUri(WebLabProcessing.HAS_GATHERING_DATE);
		lib.setVar("gathering");
		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doEndTag();

		final List<?> dates = (List<?>) this.mockPageContext.getAttribute("gathering");
		Assert.assertTrue(dates.get(0) instanceof Date);
		Assert.assertEquals(1, dates.size());
		Assert.assertTrue(((Date) dates.get(0)).equals(now));
	}


	@Test
	public void testAddAttributeTypedSimpleValue2() throws JspException, UnsupportedEncodingException {

		final PropertyReaderTagLib lib = new PropertyReaderTagLib();
		final Document doc = new Document();
		doc.setUri("http://" + UUID.randomUUID().toString());

		final WProcessingAnnotator wpa = new WProcessingAnnotator(doc);
		wpa.writeOriginalFileSize(Long.valueOf(14031983L));

		lib.setResource(doc);
		lib.setUri(WebLabProcessing.HAS_ORIGINAL_FILE_SIZE);
		lib.setVar("fileSize");
		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doEndTag();

		final List<?> sizes = (List<?>) this.mockPageContext.getAttribute("fileSize");
		Assert.assertTrue(sizes.get(0) instanceof Long);
		Assert.assertEquals(1, sizes.size());
		Assert.assertTrue(((Long) sizes.get(0)).equals(Long.valueOf(14031983L)));
		final String output = ((MockHttpServletResponse) this.mockPageContext.getResponse()).getContentAsString();
		System.out.println(output);
	}


	@Test
	public void testAddAtrributeLanguageDependentValue() throws JspException {

		final PropertyReaderTagLib lib = new PropertyReaderTagLib();
		final Document doc = new Document();
		doc.setUri("http://" + UUID.randomUUID().toString());

		final DublinCoreAnnotator dca = new DublinCoreAnnotator(doc);
		dca.writeTitle("mon titre");
		dca.startSpecifyLanguage("en");
		dca.writeTitle("my title");

		final WProcessingAnnotator wpa = new WProcessingAnnotator(doc);
		wpa.writeGatheringDate(new Date());

		this.mockPageContext.getResponse().setLocale(Locale.ENGLISH);
		lib.setResource(doc);
		lib.setUri(DublinCore.TITLE_PROPERTY_NAME);

		lib.setPageContext(this.mockPageContext);
		lib.setVar("title");

		lib.doStartTag();
		lib.doEndTag();
		Assert.assertTrue(this.mockPageContext.getAttribute("title") instanceof List<?>);

		final List<?> titles = (List<?>) this.mockPageContext.getAttribute("title");
		Assert.assertTrue(titles.get(0) instanceof String);
		Assert.assertEquals(1, titles.size());
		Assert.assertTrue(((String) titles.get(0)).contains("my title"));
	}


	@Test
	public void testAddAttributeLanguageDependentValues() throws JspException {

		final PropertyReaderTagLib lib = new PropertyReaderTagLib();
		final Document doc = new Document();
		doc.setUri("http://" + UUID.randomUUID().toString());

		final DublinCoreAnnotator dca = new DublinCoreAnnotator(doc);
		dca.writeTitle("mon titre");
		dca.startSpecifyLanguage("en");
		dca.writeTitle("my title");
		dca.writeTitle("my title2");

		final WProcessingAnnotator wpa = new WProcessingAnnotator(doc);
		wpa.writeGatheringDate(new Date());

		this.mockPageContext.getResponse().setLocale(Locale.ENGLISH);
		lib.setResource(doc);
		lib.setUri(DublinCore.TITLE_PROPERTY_NAME);
		lib.setVar("title");
		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doEndTag();

		final List<?> titles = (List<?>) this.mockPageContext.getAttribute("title");

		Assert.assertEquals(2, titles.size());
		Assert.assertTrue((titles.get(0).toString().contains("my title") && titles.get(1).toString().contains("my title2"))
				|| (titles.get(0).toString().contains("my title2") && titles.get(1).toString().contains("my title")));
	}


	@Test
	public void testAddAttributeNoLanguageDependentValues() throws JspException {

		final PropertyReaderTagLib lib = new PropertyReaderTagLib();
		final Document doc = new Document();
		doc.setUri("http://" + UUID.randomUUID().toString());

		final DublinCoreAnnotator dca = new DublinCoreAnnotator(doc);
		dca.writeTitle("mon titre");
		dca.startSpecifyLanguage("en");
		dca.writeTitle("my title");
		dca.writeTitle("my title2");

		final WProcessingAnnotator wpa = new WProcessingAnnotator(doc);
		wpa.writeGatheringDate(new Date());

		this.mockPageContext.getResponse().setLocale(Locale.FRENCH);
		lib.setResource(doc);
		lib.setUri(DublinCore.TITLE_PROPERTY_NAME);
		lib.setVar("title");
		lib.setPageContext(this.mockPageContext);

		lib.doStartTag();
		lib.doEndTag();

		final List<?> titles = (List<?>) this.mockPageContext.getAttribute("title");
		Assert.assertEquals(3, titles.size());
		Assert.assertNotEquals(titles.get(0), titles.get(1));
		Assert.assertNotEquals(titles.get(1), titles.get(2));
		Assert.assertNotEquals(titles.get(0), titles.get(2));
	}


	@Test
	public void testHasValue() throws Exception {

		final Document doc = new Document();
		doc.setUri("http://" + UUID.randomUUID().toString());

		final DublinCoreAnnotator dca = new DublinCoreAnnotator(doc);
		Assert.assertFalse(PropertyReaderTagLib.hasValue(doc, DublinCore.TITLE_PROPERTY_NAME));

		dca.startSpecifyLanguage("en");
		dca.writeTitle("my title");

		Assert.assertTrue(PropertyReaderTagLib.hasValue(doc, DublinCore.TITLE_PROPERTY_NAME));
	}


}
