/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2019 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.test;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.servlet.jsp.tagext.TagData;
import javax.servlet.jsp.tagext.VariableInfo;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.components.taglib.PropertyReaderTagLibTEI;

/**
 * Just a simple class testing TEI in order to increase coverage
 *
 * @author ymombrun
 */
public class TestTEI {


	@Test
	public void testTEI() throws Exception {

		final PropertyReaderTagLibTEI tei = new PropertyReaderTagLibTEI();
		final Map<String, String> map = new HashMap<>();
		map.put("var", "a value");
		final VariableInfo[] infos = tei.getVariableInfo(new TagData(new Hashtable<>(map)));
		Assert.assertNotNull(infos);
		Assert.assertEquals(1, infos.length);
	}

}
