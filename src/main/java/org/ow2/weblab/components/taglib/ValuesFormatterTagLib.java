/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.components.taglib;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTag;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.Tag;

import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.taglibs.standard.tag.common.fmt.BundleSupport;

/**
 * To render values in as HTML tags according to a given style. Available styles are :
 * - icon : produce image html tag from the passed value. Icons are based on the theme named 'weblab'.
 * - icon+text = produce image html tag from the passed value and a description text in span tag.
 * Icons are based on the theme named 'weblab' and span content are reading from the bundle file.
 * - text = produce a description text in span tag. The span content is reading from the bundle file.
 * - anchor = the passed value is converted in anchor tag with href filled with the passed value.
 * - anchor+forced = the passed value is converted in anchor tag with href filled with the passed value.
 * - fileSize = the passed value is converted in the more human readable value (kb, mb, gb, tb).
 *
 * @author emilienbondu
 */
public class ValuesFormatterTagLib extends BodyTagSupport {


	/** icon */
	public static final String ICON = "icon";


	/** icon+text */
	public static final String ICON_TEXT = "icon+text";


	/** text */
	public static final String TEXT = "text";


	/** fileSize */
	public static final String FILE_SIZE = "fileSize";


	/** anchor */
	public static final String ANCHOR = "anchor";


	/** anchor+forced */
	public static final String ANCHOR_FORCED = "anchor+forced";


	private static final long serialVersionUID = 5778753719469834534L;


	private static final String BUNDLE_BASE_NAME = "weblab_taglib";


	private static final String BUNDLE_EXT_NAME = "weblab_taglib_ext";


	private String style;


	private Object value;


	private String cssClass;


	private String cssId;


	private final Log log = LogFactory.getLog(this.getClass());


	@Override
	public int doStartTag() {

		// if value is null, we have to render the value from the tag body
		if (this.value != null) {
			this.renderValue(this.value);
			return Tag.SKIP_BODY;
		}
		return BodyTag.EVAL_BODY_BUFFERED;
	}


	@Override
	public int doAfterBody() {

		// value is rendered from the body content
		if (this.getBodyContent().getString().trim() != "") {
			this.renderValue(this.getBodyContent().getString().trim());
		}
		return Tag.SKIP_BODY;
	}


	/**
	 * Render the value in the output
	 *
	 * @param theValue
	 *            The value as send by doStartTag and doEndTag. Might only be a String or a list of string.
	 */
	private void renderValue(final Object theValue) {

		final StringBuffer buf = new StringBuffer();

		if (theValue == null) {
			this.log.warn("The value was null. Ignoring it.");
		} else if (theValue instanceof String) {
			this.renderSimpleString(buf, (String) theValue);
		} else if ((theValue instanceof Long) && ValuesFormatterTagLib.FILE_SIZE.equalsIgnoreCase(this.style)) {
			ValuesFormatterTagLib.generateFileSizeTextTag(buf, (Long) theValue, this.cssClass, this.cssId);
		} else if (theValue instanceof List<?>) {
			for (final Object innerValue : (List<?>) theValue) {
				if (innerValue instanceof String) {
					this.renderSimpleString(buf, (String) innerValue);
					buf.append(' ');
				} else if ((innerValue instanceof Long) && ValuesFormatterTagLib.FILE_SIZE.equalsIgnoreCase(this.style)) {
					ValuesFormatterTagLib.generateFileSizeTextTag(buf, (Long) innerValue, this.cssClass, this.cssId);
					buf.append(' ');
				} else if (innerValue != null) {
					this.renderSimpleString(buf, innerValue.toString());
					buf.append(' ');
				}
			}
		} else {
			this.log.warn("The value uses an unhandled type: " + theValue.getClass() + ". Converting it to string.");
			this.renderSimpleString(buf, theValue.toString());
		}

		try {
			if (this.value == null) {
				// value from body content, using the enclosing writer
				this.getBodyContent().getEnclosingWriter().print(buf);
			} else {
				this.pageContext.getOut().print(buf.toString());
			}
		} catch (final IOException ioe) {
			this.log.error("An error occured writing the value " + theValue + " to the page.", ioe);
		}
	}


	private void renderSimpleString(final StringBuffer buf, final String theValue) {

		switch (this.style) {
			case ValuesFormatterTagLib.ICON:
				this.generateImageTag(buf, theValue, this.cssClass, this.cssId);
				break;
			case ValuesFormatterTagLib.ICON_TEXT:
				this.generateImageTag(buf, theValue, this.cssClass, this.cssId);
				this.generateTextTag(buf, theValue, this.cssClass, this.cssId);
				break;
			case ValuesFormatterTagLib.TEXT:
				this.generateTextTag(buf, theValue, this.cssClass, this.cssId);
				break;
			case ValuesFormatterTagLib.FILE_SIZE:
				this.generateFileSizeTextTag(buf, theValue, this.cssClass, this.cssId);
				break;
			case ValuesFormatterTagLib.ANCHOR:
				this.generateAnchorTag(buf, theValue, this.cssClass, this.cssId);
				break;
			case ValuesFormatterTagLib.ANCHOR_FORCED:
				this.generateAnchorTag(buf, theValue, this.cssClass, this.cssId, true);
				break;
			default:
				this.log.warn("Unhandled style: " + this.style);
				break;
		}
	}


	/**
	 * Generate anchor tag in the buffer. Only create a link if the value starts with http(s):// or ftp(s)://
	 *
	 * @param buffer
	 *            The buffer to write into
	 * @param val
	 *            The value to be used as link content
	 * @param theCssClass
	 *            The class of the link
	 * @param theCssId
	 *            The id of the span
	 */
	private void generateAnchorTag(final StringBuffer buffer, final String val, final String theCssClass, final String theCssId) {

		this.generateAnchorTag(buffer, val, theCssClass, theCssId, false);
	}


	/**
	 * @param buffer
	 *            The buffer of the page to write into
	 * @param val
	 *            The value of the property
	 * @param theCssClass
	 *            The class of the link
	 * @param theCssId
	 *            The id of the span
	 */
	private void generateAnchorTag(final StringBuffer buffer, final String val, final String theCssClass, final String theCssId, final boolean forced) {

		if (val.trim().isEmpty()) {
			buffer.append(val);
		} else if (forced || val.trim().startsWith("http://") || val.trim().startsWith("https://") || val.trim().startsWith("ftp://") || val.trim().startsWith("ftps://")) {
			buffer.append("<a");
			ValuesFormatterTagLib.appendCSSAttributes(buffer, theCssClass, theCssId);
			buffer.append(" href='");
			buffer.append(val);
			buffer.append("'  target='_blank' >");
			buffer.append(val);
			buffer.append("</a>");
		} else {
			this.log.debug("Do not create an anchor for non network value:" + val + ".");
			buffer.append(val);
		}
	}


	/**
	 * Generate span tag for a string file size value
	 *
	 * @param buffer
	 * @param val
	 * @param theCssClass
	 * @param theCssId
	 * @throws JspException
	 */
	private void generateFileSizeTextTag(final StringBuffer buffer, final String val, final String theCssClass, final String theCssId) {

		try {
			ValuesFormatterTagLib.generateFileSizeTextTag(buffer, Long.valueOf(val), theCssClass, theCssId);
		} catch (final NumberFormatException e) {
			this.log.error("The value '" + val + "' is not parsable as a Long to generate a valid human readable file size.");
			this.log.trace(e, e);
		}
	}


	/**
	 * Generate span tag for a long file size value
	 *
	 * @param buffer
	 * @param val
	 * @param theCssClass
	 * @param theCssId
	 */
	private static void generateFileSizeTextTag(final StringBuffer buffer, final Long val, final String theCssClass, final String theCssId) {

		buffer.append("<span");
		ValuesFormatterTagLib.appendCSSAttributes(buffer, theCssClass, theCssId);
		buffer.append(">");
		buffer.append(ValuesFormatterTagLib.byteSizeHumanReadable(val.longValue()));
		buffer.append("</span>");
	}


	/**
	 * Generate span tag for a value
	 *
	 * @param buffer
	 * @param theValue
	 * @param theCssClass
	 * @param theCssId
	 */
	private void generateTextTag(final StringBuffer buffer, final String theValue, final String theCssClass, final String theCssId) {

		buffer.append("<span");
		ValuesFormatterTagLib.appendCSSAttributes(buffer, theCssClass, theCssId);
		buffer.append(">");

		final ResourceBundle bundle = this.loadBundle();
		final Locale locale = this.toLocaleSafe(theValue);

		if ((locale != null) && bundle.containsKey("taglib.icon." + locale.getLanguage()) && bundle.containsKey("taglib.text." + locale.getLanguage())) {
			buffer.append(bundle.getString("taglib.text." + locale.getLanguage()));
		} else if (bundle.containsKey("format." + theValue)) {
			buffer.append(bundle.getString("taglib.text." + bundle.getString("format." + theValue)));
		} else if (bundle.containsKey("type." + theValue)) {
			buffer.append(bundle.getString("taglib.text." + bundle.getString("type." + theValue)));
		} else if (theValue.equalsIgnoreCase("und")) {
			buffer.append(bundle.getString("taglib.text.unknown"));
		} else {
			this.log.debug("Unhandled language or not a language, just a standard string value: " + theValue);
			buffer.append(theValue);
		}

		buffer.append("</span>");
	}


	/**
	 * To append CSS attributes to the buffer
	 *
	 * @param buffer
	 * @param theCssClass
	 * @param theCssId
	 */
	private static void appendCSSAttributes(final StringBuffer buffer, final String theCssClass, final String theCssId) {

		if (theCssClass != null) {
			buffer.append(" class='" + theCssClass + "'");
		}
		if (theCssId != null) {
			buffer.append(" id='" + theCssId + "'");
		}
	}


	/**
	 * Generate image tag for a value
	 *
	 * @param buffer
	 * @param theValue
	 * @param theCssClass
	 * @param theCssId
	 */
	private void generateImageTag(final StringBuffer buffer, final String theValue, final String theCssClass, final String theCssId) {

		buffer.append("<img");
		ValuesFormatterTagLib.appendCSSAttributes(buffer, theCssClass, theCssId);

		final ResourceBundle bundle = this.loadBundle();
		final Locale locale = this.toLocaleSafe(theValue);

		if ((locale != null) && bundle.containsKey("taglib.icon." + locale.getLanguage()) && bundle.containsKey("taglib.text." + locale.getLanguage())) {
			ValuesFormatterTagLib.appendImageAttributesFromBundle(bundle, buffer, locale.getLanguage());
		} else if (bundle.containsKey("format." + theValue)) {
			ValuesFormatterTagLib.appendImageAttributesFromBundle(bundle, buffer, bundle.getString("format." + theValue));
		} else if (theValue.equalsIgnoreCase("und")) {
			ValuesFormatterTagLib.appendImageUnknownAttributes(bundle, buffer, bundle.getString("taglib.text.unknown"));
		} else {
			ValuesFormatterTagLib.appendImageUnknownAttributes(bundle, buffer, theValue);
		}

		buffer.append("/>");
	}


	private Locale toLocaleSafe(final String potentialLocaleName) {

		final Locale loc;
		try {
			loc = LocaleUtils.toLocale(potentialLocaleName);
		} catch (final Exception e) {
			this.log.trace(e, e);
			// Do not care
			return null;
		}
		return loc;
	}


	private static void appendImageUnknownAttributes(final ResourceBundle bundle, final StringBuffer buffer, final String format) {

		buffer.append(" src='" + bundle.getString("taglib.icon.unknown") + "'");
		buffer.append(" alt='" + format + "'");
		buffer.append(" title='" + format + "'");
	}


	private static void appendImageAttributesFromBundle(final ResourceBundle bundle, final StringBuffer buffer, final String key) {

		buffer.append(" src='" + bundle.getString("taglib.icon." + key) + "'");
		buffer.append(" alt='" + bundle.getString("taglib.text." + key) + "'");
		buffer.append(" title='" + bundle.getString("taglib.text." + key) + "'");
	}


	/**
	 * @return The resource bundle loaded. First from weblab_taglig_ext and if not found from weblab_taglig (which is shipped with the jar)
	 */
	private ResourceBundle loadBundle() {

		final ResourceBundle bundleExt = BundleSupport.getLocalizationContext(this.pageContext, ValuesFormatterTagLib.BUNDLE_EXT_NAME).getResourceBundle();
		if (bundleExt == null) {
			return BundleSupport.getLocalizationContext(this.pageContext, ValuesFormatterTagLib.BUNDLE_BASE_NAME).getResourceBundle();
		}
		return bundleExt;
	}


	/**
	 * @param value
	 *            The number of bytes to convert into a human readable value (KB, MB...)
	 * @return The humane readable value
	 */
	public static String byteSizeHumanReadable(final long value) {

		final String[] dictionary = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB" };
		int index = 0;
		double displayCount = value;
		for (index = 0; index < dictionary.length; index++) {
			if (displayCount < 1024) {
				break;
			}
			displayCount = displayCount / 1024;
		}
		return new DecimalFormat("0.##").format(Double.valueOf(displayCount)) + " " + dictionary[index];
	}


	/**
	 * @return the style
	 */
	public String getStyle() {

		return this.style;
	}


	/**
	 * @param style
	 *            the style to set
	 */
	public void setStyle(final String style) {

		this.style = style;
	}


	/**
	 * @return the value
	 */
	public Object getValue() {

		return this.value;
	}


	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(final Object value) {

		this.value = value;
	}


	/**
	 * @return the cssClass
	 */
	public String getCssClass() {

		return this.cssClass;
	}


	/**
	 * @param cssClass
	 *            the cssClass to set
	 */
	public void setCssClass(final String cssClass) {

		this.cssClass = cssClass;
	}


	/**
	 * @return the cssId
	 */
	public String getCssId() {

		return this.cssId;
	}


	/**
	 * @param cssId
	 *            the cssId to set
	 */
	public void setCssId(final String cssId) {

		this.cssId = cssId;
	}

}
