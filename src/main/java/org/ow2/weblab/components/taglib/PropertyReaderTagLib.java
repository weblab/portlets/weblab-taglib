/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.components.taglib;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.Tag;

import org.ow2.weblab.core.annotator.AbstractAnnotator.Operator;
import org.ow2.weblab.core.annotator.BaseAnnotator;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.rdf.RDFDatatypeContainer;
import org.ow2.weblab.rdf.Value;

/**
 * This taglib allows to read RDF properties values about a WebLab resource.
 * The value can be written in the page output or set in a page variable.
 *
 * @author emilienbondu
 *
 */
public class PropertyReaderTagLib extends BodyTagSupport {


	/**
	 * Attributes
	 */
	private static final long serialVersionUID = 5778753719469834534L;


	private static final String PAGE_RESOURCE_ATTRIBUTE_NAME = "resource";


	private Resource resource = null;


	private String uri = null;


	private String var = null;


	@Override
	public int doStartTag() throws JspException {
		// set the WebLab resource from page context or the passed attribute
		if ((this.resource == null) && (this.pageContext.getRequest().getAttribute(PropertyReaderTagLib.PAGE_RESOURCE_ATTRIBUTE_NAME) != null)) {
			this.resource = (Resource) this.pageContext.getRequest().getAttribute(PropertyReaderTagLib.PAGE_RESOURCE_ATTRIBUTE_NAME);
		}

		// reading value
		final Value<RDFDatatypeContainer> value = PropertyReaderTagLib.readValue(this.resource, this.uri);

		if (value.hasValue()) {
			final String locale = this.pageContext.getResponse().getLocale().getLanguage();
			// if using the var attribute, adding the value as a page attribute otherwise, writing the value in the output
			if (this.var != null) {
				// values in page context
				final List<Object> values = new ArrayList<>();
				if (value.getValues(locale) != null) {

					for (final RDFDatatypeContainer val : value.getValues(locale)) {
						values.add(val.getObject());
					}
				} else {
					for (final RDFDatatypeContainer val : value.getValues()) {
						values.add(val.getObject());
					}
				}
				this.pageContext.setAttribute(this.var, values);
			} else {
				// values in the output
				try {
					if (value.getValues(locale) != null) {
						for (final RDFDatatypeContainer val : value.getValues(locale)) {
							this.pageContext.getOut().print(val.getObject().toString());
						}
					} else {
						for (final RDFDatatypeContainer val : value.getValues()) {
							this.pageContext.getOut().print(val.getObject().toString());
						}
					}
				} catch (final IOException ioe) {
					throw new JspException("IO Exception when trying to pring the values.", ioe);
				}
			}
		}
		return Tag.SKIP_BODY;
	}


	@Override
	public int doEndTag() throws JspException {
		// reset attributes for next call
		this.resource = null;
		this.uri = null;
		this.var = null;
		return super.doEndTag();
	}


	/**
	 * To check if a given WebLab resource has a value for a given RDF property
	 *
	 * @param resource
	 *            The resource to look into
	 * @param uri
	 *            The uri of the property to read
	 * @return true if a value has been found false otherwise
	 */
	public static boolean hasValue(final Resource resource, final String uri) {
		return (!PropertyReaderTagLib.readValue(resource, uri).getValues().isEmpty());
	}


	/**
	 * To read values for a given property URI by using the base annotator
	 *
	 * @param resource
	 * @param uri
	 * @return
	 */
	private static Value<RDFDatatypeContainer> readValue(final Resource resource, final String uri) {
		final BaseAnnotator annotator = new BaseAnnotator(resource);
		final URI property = URI.create(uri);
		// call annotator
		return annotator.applyOperator(Operator.READ, null, property, RDFDatatypeContainer.class, null);
	}


	/**
	 * @return the resource
	 */
	public Resource getResource() {
		return this.resource;
	}


	/**
	 * @param resource
	 *            the resource to set
	 */
	public void setResource(final Resource resource) {
		this.resource = resource;
	}


	/**
	 * @return the uri
	 */
	public String getUri() {
		return this.uri;
	}


	/**
	 * @param uri
	 *            the uri to set
	 */
	public void setUri(final String uri) {
		this.uri = uri;
	}


	/**
	 * @return the var
	 */
	public String getVar() {
		return this.var;
	}


	/**
	 * @param var
	 *            the var to set
	 */
	public void setVar(final String var) {
		this.var = var;
	}

}
